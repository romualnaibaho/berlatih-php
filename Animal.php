<?php
    class Animal {
        private $name;
        private $legs;
        private $cold_blooded;

        function __construct($name) {
            $this->name = $name;
            $this->legs = 2;
            $this->cold_blooded = "false";
        }

        function get_name() {
            echo $this->name;
        }

        function get_legs() {
            echo $this->legs;
        }

        function get_cold_blooded() {
            echo $this->cold_blooded;
        }

        function set_name($name) {
            $this->name = $name;
        }

        function set_legs($legs) {
            $this->legs = $legs;
        }

        function set_cold_blooded($cold_blooded) {
            $this->cold_blooded = $cold_blooded;
        }
    }
?> 