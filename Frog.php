<?php
    class Frog extends Animal {

        function __construct($name) {
            $this->name = $name;
            $this->legs = 2;
            $this->cold_blooded = "false";
        }

        function jump(){
            echo "Hop Hop";
        }
    }
?> 