<?php

    require 'Animal.php';
    require 'Ape.php';
    require 'Frog.php';

    echo "<h2>Release 0</h2>";

    $sheep = new Animal("shaun");

    echo $sheep->get_name(); // "shaun"
    echo "<br>";
    echo $sheep->get_legs(); // 2
    echo "<br>";
    echo $sheep->get_cold_blooded(); // false
    echo "<br>";

    //--------------------------------------------------
    //Release 1
    echo "<h2>Release 1</h2>";
    $sungokong = new Ape("kera sakti");
    $sungokong->yell(); // "Auooo"

    echo "<br>";

    $kodok = new Frog("buduk");
    $kodok->jump() ; // "hop hop"

?>